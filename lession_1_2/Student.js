"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Student = exports.PI = void 0;
var Student = /** @class */ (function () {
    function Student(id, name) {
        this.id = id;
        this.name = name;
    }
    Student.prototype.getName = function () {
        return this.name;
    };
    Student.prototype.setName = function (name) {
        this.name = name;
    };
    return Student;
}());
exports.Student = Student;
var PI = 3.14;
exports.PI = PI;
