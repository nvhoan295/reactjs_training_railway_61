var helloWorld: string = "Hello World";
if (true) {
    var helloWorld: string = "Hello World in if";
}
console.log(helloWorld);

// let helloWorldLet : string = "Hello World let";
// if (true) {
//     let helloWorldLet : string = "Hello World let in if";
// }

const hello1: Number[] = [1, 2, 3, 4];
// hello[0] = 3;
console.log(hello1);

var now = Date()
console.log(now);
function show(info: {id: number, name: string, age: number}) {
    console.log("id", info.id);
    console.log("name", info.name);
    console.log("age", info.age);
}

var thang = {id: 1, name: "luu quoc thang", age: 18, address: "Dai Mo"};
var trang = {id: 2, name: "luu phuong trang", age: 17};
show(thang);
show(trang);