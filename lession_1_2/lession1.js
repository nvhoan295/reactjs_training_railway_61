// var hello;
// console.log(hello);
// hello = "hello";

console.log(hello);
var hello = "hello1";

let count = 2;
// let count = 1;
let hello_let;
console.log(hello_let);
hello_let = "hello_let";

function add() {
    var count_1 = 1;
    count_1 += count;
    console.log("count in add:", count);
    console.log("count_1 in add:", count_1);
}

function add_1(thang, age) {
    var count_1 = 1;
    count_1 += count;
    console.log("count in add:", count);
    console.log("count_1 in add:", count_1);
    return thang + age;
}

add();
// console.log("count_1 out add:", count_1);

var persion = {
    fisrt_name: "thang",
    age: 18,
    address: ["Dai Mo", "Ha Dong", "Cau Giay", "Ba Dinh", "Viet Nam"],
    info: function (name, age) {
        console.log("My name", name);
        console.log("age", age);
        console.log("fisrt_name", this.fisrt_name);
    },
    clazz: {
        no: "61",
        number_student: "15"
    }
}
console.log(persion.info("luu quoc thang", 18));
var {
    fisrt_name: name1,
    address: [address1, address2, address3],
    info: funcA,
    age: age1,
    clazz: { no: no_1, number_student: num_s }
} = persion;
console.log("name1", name1);
console.log("address3", address3);
// console.log("age1", age1);
// funcA("thang", 18);
console.log("no_1", no_1);

var str1 = "string 1 has \" and '";
var str2 = 'string 2 has " and \'';
var str3 = `
    Profile:
        - Name: ${name1}
        - Age: ${age1}
        - Address: ${address1}, ${address2}, ${address3}
`;



var sum1 = function (num1, num2) {
    return num1 + num2;
}
var { address: address_arr } = persion;
console.log("str1", str1);
console.log("str2", str2);
console.log("str3", str3);
console.log("address_arr", address_arr);
var address_arr_1 = [...address_arr];
var persion_1 = { ...persion, other: "other" };
address_arr[0] = "Nam Tu Liem";
console.log("address_arr_1", address_arr_1);
console.log("persion_1", persion_1);


var sum2 = (num1, num2) => {
    return num1 + num2;
}

var sum3 = (num1, num2, num3) => {
    return num1 + num2 + num3;
}

var sum3 = (arr) => {
    let sum = 0;
    arr.forEach(element => {
        sum += element;
    });
    return sum
}

double = (x) => {
    return x * 2
}
console.log(sum1(3.4, "thang", 6, 7, 8));
console.log(sum2(3.4, "thang", 6, 7, 8));

function greeting(name, age) {
    console.log(`Hello, ${name} ${age}`);
}

function processUserInput(abc, abc1, name, age) {
    age -= 1;
    abc1();
    abc();
}

processUserInput(greeting, greeting, "luu quoc thang", 19);

var address_arr_2 = address_arr_1.filter(
    (x, y, z) => {
        console.log(y, x.length, z);
        return x.length < 8;
    }
)

console.log("address_arr_2", address_arr_2);

var mapping = (value) => {return "Address " + value};
const address_arr_3 = address_arr_1.map(mapping);

console.log("address_arr_3", address_arr_3);

console.log("Sum", sum3([1, 2, 3, 4, 5]));

console.log( Math.max(1, 20, 3, 4, 5) );

function sum4(num1,num2,num3, ...abc) { // abc is the name for the array
    let sum = 0;
  
    abc.forEach((element) => sum += element)
  
    return sum + num1 + num2 + num3;
  }

  console.log("sum4", sum4(1, 2, 3, 4, 5, 7, 8, 9, 1000, 200));

  function show(info) {
    console.log("id", info.id);
    console.log("name", info.name);
    console.log("age", info.age);
    console.log("city", info.address?.city);
    console.log("country", info.address?.country);
}

var thang = {id: "1", name: "luu quoc thang", age: 18, address: {city: "Ha Noi", country: "VN"}};
var trang = {id: 2, name: "luu phuong trang", age: 17};
var thang_1 = {name: "luu quoc thang", age: 18, address: "Dai Mo"};
show(thang);
show(trang);
show(thang_1);