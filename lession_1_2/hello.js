var helloWorld = "Hello World";
if (true) {
    var helloWorld = "Hello World in if";
}
console.log(helloWorld);
// let helloWorldLet : string = "Hello World let";
// if (true) {
//     let helloWorldLet : string = "Hello World let in if";
// }
var hello1 = [1, 2, 3, 4];
// hello[0] = 3;
console.log(hello1);
var now = Date();
console.log(now);
function show(info) {
    console.log("id", info.id);
    console.log("name", info.name);
    console.log("age", info.age);
}
var thang = { id: 1, name: "luu quoc thang", age: 18, address: "Dai Mo" };
var trang = { id: 2, name: "luu phuong trang", age: 17 };
show(thang);
show(trang);
