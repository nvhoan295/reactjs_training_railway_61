var helloWorld = "Hello World";
if (true) {
    var helloWorld = "Hello World in if";
}
console.log(helloWorld);
// let helloWorldLet : string = "Hello World let";
// if (true) {
//     let helloWorldLet : string = "Hello World let in if";
// }
var hello = [1, 2, 3, 4];
hello[0] = 3;
console.log(hello);
var now = Date();
console.log(now);
var Student = /** @class */ (function () {
    function Student(id, name) {
        this.id = id;
        this.name = name;
    }
    Student.prototype.getName = function () {
        return this.name;
    };
    Student.prototype.setName = function (name) {
        this.name = name;
    };
    return Student;
}());
