let promise = new Promise(function (resolve, reject) {
    // the function is executed automatically when the promise is constructed

    // after 1 second signal that the job is done with the result "done"
    setTimeout(
        () => {
            console.log("run");
            return resolve(123);
            // return reject("there is a error!");
        },
        5000
    );
});

console.log("promise", promise);

// resolve runs the first function in .then
promise.then(
    result => {
        throw new Error("Whoops!");
        alert(a);
    }, // shows "done!" after 1 second
    error => alert(error) // doesn't run
)
.catch(alert);