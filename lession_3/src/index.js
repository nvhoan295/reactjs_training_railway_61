import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import HomePape from "./components/HomePage";
import PostDataList from "./components/PostDataList";
import AddPost from "./components/AddPost";
import Post from "./components/Post";
import PostWithContext from "./components/PostWithList";
import PostWithList from "./components/PostWithList";
import Page404 from "./components/Page404";
import BasePost from "./components/BasePost";
import Count from "./components/Count";
import Login from "./components/Authen/Login";
import RequiredAuth from "./components/Authen/RequiredAuth";
import { Provider } from "react-redux";
import rootStore, { persistor } from "./store/store";
import { PersistGate } from "redux-persist/integration/react";
import BaseLayout from "./components/shared/BaseLayout";
import Register from "./components/Register/Register";
import Profile from "./components/Profile/Profile";
import User from "./components/User";

const root = ReactDOM.createRoot(document.getElementById("root"));
const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        path: "/home",
        element: <BaseLayout />,
        children: [
          {
            path:"",
            element: <HomePape></HomePape>
          }, 
          {
            path: "post",
            element: <BasePost />,
            children: [
              {
                path: "",
                element: <PostDataList />,
              },
              {
                path: ":postId",
                element: <PostWithList />,
              },
              {
                path: "adds",
                element: <AddPost />,
              },
            ],
          },
          {
            path: "count",
            element: <Count></Count>,
          },
          {
            path: "user",
            element: (
              <RequiredAuth>
                <User></User>
              </RequiredAuth>
            ),
            // element: <Student></Student>
          },
          {
            path: "user/:id",
            element: (
              <RequiredAuth>
                <Profile></Profile>
              </RequiredAuth>
            ),
            // element: <Student></Student>
          },
        ],
      },
      {
        path: "login",
        element: <Login />,
      },
      {
        path: "404",
        element: <Page404 />,
      },
      {
        path: "*",
        element: <Page404 />,
      },
      {
        path: "register",
        element: <Register />,
      },
      // {
      //   path: "/posts/:id",
      //   element: <Post />,
      // },
    ],
  },
]);

root.render(
  <React.StrictMode>
    <Provider store={rootStore}>
      <PersistGate loading={<></>} persistor={persistor}>
        <RouterProvider router={router}>{/* <App /> */}</RouterProvider>
      </PersistGate>
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
