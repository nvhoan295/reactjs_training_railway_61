import { DeleteOutlined } from "@ant-design/icons";
import { Button, Space, Table, Tag } from "antd";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import api from "../services/api";
import { BASE_URL } from "../services/baseService";

function User() {
  const [students, setStudent] = useState([]);
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      render: (text, record) => <Link to={record.id}>{text}</Link>,
    },
    {
      title: "Age",
      dataIndex: "age",
      key: "age",
    },
    {
      title: "Phone Number",
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Tags",
      key: "tags",
      dataIndex: "tags",
      render: (_, record) => (
        <>
          {record.tags.map((tag) => {
            let color = tag.length > 5 ? "geekblue" : "green";
            if (tag === "loser") {
              color = "volcano";
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Button icon={<DeleteOutlined />} type="link" danger>
            Delete
          </Button>
        </Space>
      ),
    },
  ];

  useEffect(() => {
    const fetchStudent = async () => {
      const res = await api.get(BASE_URL + "/students", {
        page: 1,
        pageSize: 15,
      });
      if (res) {
        setStudent(res);
      }
    };
    fetchStudent();
  }, []);

  const paginationObj = {};
  return (
    <>
      <Table
        columns={columns}
        dataSource={students}
        pagination={{
          position: ["topRight", "bottomRight"],
        //   pageSize: 10,
          total: students.length,
          showSizeChanger: true,
          showTotal: (total, range) =>`${range[0]}-${range[1]} of Total: ${total} items`,
        //   defaultPageSize: 5,
          defaultCurrent: 1,
        }}
      />
    </>
  );
}
export default User;
