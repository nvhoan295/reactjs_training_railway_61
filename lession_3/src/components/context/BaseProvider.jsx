import { createContext } from "react";
import { useState } from "react";

export const BaseContext = createContext();
BaseContext.displayName = 'BaseContextAA';
function BaseProvider(props) {
    const [lang, setLang] = useState("vn");
    const token = localStorage.getItem("token");
    const login = token ? true : false;
    const [isLoggin, setLogin] = useState(login);

    return <BaseContext.Provider value={{lang, setLang, isLoggin, setLogin}}>
        {props.children}
    </BaseContext.Provider>
    
}

export default BaseProvider;