import styled from "styled-components";

const HeaderStyle = styled.header`
    display: flex;
    padding: 10px;
    justify-content: space-between;
`;
const DropDownLanguage = styled.div`
    position: relative;
`;

const SelectBox = styled.select`
    position: absolute;
    background-color: #f1f1f1;
    min-width: 40px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    right: 0;
    z-index: 1;
`

const NameDiv = styled.div`
    display: inline-flex;
`
export {
    HeaderStyle,
    DropDownLanguage,
    SelectBox,
    NameDiv
}