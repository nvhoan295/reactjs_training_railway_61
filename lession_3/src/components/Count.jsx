import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { configContentSelector, configSelector } from "../store/selectors/configSelector";
import { addNumsConfigAction, changeContentConfigAction, changeNumberFuncConfigAction, changeNumsConfigAction, decreNumberFuncConfigAction, increNumberFuncConfigAction } from "../store/actions/configActionType";
import { counterSelector } from "../store/selectors/counterSelector";
import { decrement, increment, incrementByAmount } from "../store/reducers/counterReducer";
import { getValue1Api, incrementAsyn } from "../store/actions/counterAsynAction";
import { callApiValue1Action } from "../store/actions/counterActionType";
import { Button } from "antd";
import {UpSquareOutlined, DownSquareOutlined} from "@ant-design/icons"

function Count() {

    const [changeNumber, setChangeNumber] = useState(0);
    const [amount, setAmount] = useState(0);
    const store = useSelector(configSelector);
    const counterStore = useSelector(counterSelector) 
    const contentInStore = useSelector(configContentSelector);
    const dispatch = useDispatch();
    const inputRef = useRef(null);
    console.log(store);
    let a = {
        config: {
            content: "this is data",
            numberFunc: 9,
            nums: [1, 13, 7, 19, 8, 11, 22, 1, 9, 94, 1, 5, 98]
        },
        auth: {
            token: "",
            user: "",
        }
    }


    const[count, setCount] = useState(0);
    const[count1, setCount1] = useState(0);
    console.log("Count", count);
    useEffect(() => {
        console.log("call use Effect")
        let date = new Date();
        setCount(date.getDate());
        return () => {
            console.log("Count will be unmount")
        }
    }, []);

    const changeContent = () => {
        let str = "new content" + store.numberFunc;
        dispatch({type: changeContentConfigAction, payload: str});
    }

    const increNumber = () => {
        dispatch({type: increNumberFuncConfigAction});
    }

    const decreNumber = () => {
        dispatch({type: decreNumberFuncConfigAction});
    }

    const addNumsHandle = () => {
        dispatch({type: addNumsConfigAction, payload: 1});
    }

    const changeNumsHandle = () => {
        dispatch({type: changeNumsConfigAction, payload: {a1: 13, bcd: 10}});
    }

    const changeNumberInputHandle = (evt) => {
        setChangeNumber(evt.target.value);
    }

    const changeNumberHandle = () => {
        dispatch({type: changeNumberFuncConfigAction, payload: changeNumber});
    }

    const incrementCouterHandle = () => {
        dispatch(increment());
    }

    const decrementCouterHandle = () => {
        dispatch(decrement());
    }

    const incrementByAmountCouterHandle = () => {
        dispatch(incrementByAmount(amount));
    }

    const changeAmountHandle = (e) => {
        setAmount(e.target.value);
    } 

    const incrementByAmountSyncCouterHandle = () => {
        incrementAsyn(amount, dispatch);
    }
    
    const callApiCouterHandle = () => {
        // dispatch({type: callApiValue1Action});
        dispatch(getValue1Api());
    }

    const showInputValue = () => {
        inputRef.current.focus();
        alert(inputRef.current.value);
    }
    return <>
        <p>Content: <strong>{contentInStore}</strong></p>
        <Button onClick={changeContent}>Change text</Button>
        <p>Number of Function: <strong>{store.numberFunc}</strong></p>
        <Button onClick={increNumber} icon={<UpSquareOutlined />} shape="circle"></Button>
        <Button onClick={decreNumber} icon={<DownSquareOutlined />} shape="circle"></Button>
        <Button onClick={changeNumberHandle}>Change number</Button>
        <input type="number" value={changeNumber} onChange={changeNumberInputHandle}></input>
        <p>Arr: <strong>{store.nums.toString()}</strong></p>
        <Button onClick={addNumsHandle}>add</Button><Button>delete</Button><Button onClick={changeNumsHandle}>Change</Button>
        <p>Count {count}</p>
        <Button onClick={() => {setCount(count + 1)}}>Click me</Button>
        <Button onClick={() => {setCount1(count1 + 1)}}>Click me for count1</Button>

        <p>Counter in Store: {counterStore.value} - {counterStore.value1}</p>
        <Button onClick={incrementCouterHandle}>+</Button>
        <Button onClick={decrementCouterHandle}>-</Button>
        <Button onClick={incrementByAmountCouterHandle}>Add</Button>
        <Button onClick={incrementByAmountSyncCouterHandle}>Sync Add</Button>
        <Button onClick={callApiCouterHandle}>Call API Get Value</Button>
        <input type="number" value={amount} onChange={changeAmountHandle}></input>
        <hr></hr>
        <Button onClick={showInputValue}>ShowInfoInput</Button>
        <input type="text" ref={inputRef}></input>
    </>
}

export default Count;