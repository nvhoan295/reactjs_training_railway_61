import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";
import { tokenAuthSelector } from "../../store/selectors/authSelector";

function RequiredAuth(props) {
    // const token = localStorage.getItem("token");
    const token = useSelector(tokenAuthSelector)
    if (!token) {
        return <Navigate to="/login"></Navigate>
    } else {
        return props.children;
    }
}

export default RequiredAuth;