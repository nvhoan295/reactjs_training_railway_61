import { useContext, useState } from "react";
import api from "../../services/api";
import { BASE_URL } from "../../services/baseService";
import { useNavigate } from "react-router-dom";
import { BaseContext } from "../context/BaseProvider";
import { useDispatch } from "react-redux";
import { loginApiAction } from "../../store/actions/authAction";
import { Input, Checkbox, Button, Form } from "antd";

function Login() {
  const context = useContext(BaseContext);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onFinish = async (values) => {
    console.log(values);
    // let res = await api.post(BASE_URL + "/auth", {userName: user, passWord: pass });
    // console.log("loginHandle", res);
    // if(res) {
    //     localStorage.setItem("token", res.token);
    //     localStorage.setItem("user",  JSON.stringify(res.user));
    //     context.setLogin(true);
    //     navigate("/home");
    // }
    dispatch(loginApiAction({ userName: values.username, passWord: values.password }))
      .unwrap()
      .then((originalPromiseResult) => {
        navigate("/home");
      })
      .catch((rejectedValueOrSerializedError) => {
        // handle error here
      });
  };

  const onFinishFailed = () => {

  }

  const onRegister = () => {
    navigate("/register");
  }

  return (
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      style={{
        maxWidth: 600,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="username"
        rules={[
          {
            required: true,
            message: "Please input your username!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="remember"
        valuePropName="checked"
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          Login
        </Button>
        <Button type="link" htmlType="button" onClick={onRegister}>
          Register
        </Button>
      </Form.Item>
    </Form>
  );
}

export default Login;
