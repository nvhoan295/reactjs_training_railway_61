import { useContext } from "react";
import { BaseContext } from "../context/BaseProvider";
import { Layout } from "antd";

function Footer() {
    const {Footer} = Layout;
    const context = useContext(BaseContext);
    return <Footer>made by l2t ({context.lang})</Footer>
}

export default Footer;