import {
    AntDesignOutlined,
  FormOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
import { Button, Layout, Menu, theme } from "antd";
import { useState } from "react";
import "./baseLayout.css";
import Footer from "./Footer";
import { Link, Outlet } from "react-router-dom";
import Header from "./Header";

const { Sider, Content } = Layout;

const BaseLayout = () => {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed} className="menu">
       
        <div className="demo-logo-vertical logo">
          <Link to="/home" >
            {collapsed ? <AntDesignOutlined /> : "Logo"}
          </Link>
        </div>

        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={["1"]}
          items={[
            {
              key: "1",
              icon: <FormOutlined />,
              label: (
                <Link key="post" to="post">
                  Posts
                </Link>
              ),
            },
            {
              key: "2",
              icon: <VideoCameraOutlined />,
              label: (
                <Link key="count" to="count">
                  Count
                </Link>
              ),
            },
            {
              key: "3",
              icon: <UserOutlined />,
              label: (
                <Link key="user" to="user">
                  User
                </Link>
              ),
            },
          ]}
        />
      </Sider>
      <Layout>
        <Header collapsed={collapsed} setCollapsed={setCollapsed}></Header>
        <Content
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
          }}
        >
          <Outlet></Outlet>
        </Content>
        <Footer></Footer>
      </Layout>
    </Layout>
  );
};
export default BaseLayout;
