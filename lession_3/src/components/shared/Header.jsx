import { useContext, useEffect, useState } from "react";
import { BaseContext } from "../context/BaseProvider";
import {
  NameDiv,
} from "../HeaderStyle";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  accountAuthSelector,
  tokenAuthSelector,
} from "../../store/selectors/authSelector";
import { logoutAction } from "../../store/reducers/authReducer";
import { Button, Dropdown, Layout, Space, theme } from "antd";
import { GlobalOutlined, LogoutOutlined, MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
import "./baseLayout.css";

function Header(props) {
  const { Header } = Layout;
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const [language, setLanguage] = useState("en");
  const [isShowLangDropDown, setShowLanguageDropDown] = useState(false);
  const context = useContext(BaseContext);
  const token = useSelector(tokenAuthSelector);
  const isLoggin = token ? true : false;
  const account = useSelector(accountAuthSelector);
  const dispatch = useDispatch();

  useEffect(() => {
    setLanguage(context.lang);
  }, []);

  const changeLanguage = ({ key }) => {
    setLanguage(key);
    context.setLang(key);
  };

  const showLanguageDropDown = (evt) => {
    let temp = !isShowLangDropDown;
    setShowLanguageDropDown(temp);
  };

  const logout = () => {
    localStorage.clear();
    // let value = this.context;
    // value.setLogin(false);
    dispatch(logoutAction());
  };

  const items = [
    {
      label: 'English',
      key: 'en'
    },
    {
      label: 'Japanese',
      key: 'jp'
    },
    {
      label: 'VietNamese',
      key: 'vn'
    },
  ];

  return (
    <Header
      className="header"
      style={{
        padding: 0,
        background: colorBgContainer,
      }}
    >
      <Button
        type="text"
        icon={props.collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
        onClick={() => props.setCollapsed(!props.collapsed)}
        style={{
          fontSize: "16px",
          width: 64,
          height: 64,
        }}
      />
      <NameDiv>
        <Dropdown
          menu={{
            items,
            onClick: changeLanguage,
          }}
        >
          <a onClick={showLanguageDropDown}>
            <Space>
              <GlobalOutlined className="global"/>
            </Space>
          </a>
        </Dropdown>
        
        {isLoggin ? (
          <div>
            {" "}
            <p>
              Hello, {account}{" "}
              <LogoutOutlined onClick={() => {
                  logout();
                }}/>
            </p>{" "}
          </div>
        ) : (
          <Link to="/login">Login</Link>
        )}
        
      </NameDiv>
    </Header>
  );
}
export default Header;
