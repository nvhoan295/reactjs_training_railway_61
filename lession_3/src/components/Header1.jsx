import React, { useContext } from "react";
import { BaseContext } from "./context/BaseProvider";
import { Link } from "react-router-dom";
import { DropDownLanguage, HeaderStyle, NameDiv, SelectBox } from "./HeaderStyle";

class Header1 extends React.Component {
    // const baseContext = useContext(BaseContext);
    static contextType = BaseContext;
    constructor(props) {
        super(props);
        this.state = {
            language: "en",
            isShowLangDropDown: false
        }
    }
    componentDidMount() {
        const value = this.context
        this.setState({ language: value.lang })
        console.log(value)
    }

    componentDidUpdate() {
        let value = this.context;
        console.log(value)
    }

    languageChangeValue = (evt) => {
        this.setState({ language: evt.target.value });
        let value = this.context;
        value.setLang(evt.target.value);

    }

    showLanguageDropDown = (evt) => {
        let temp = !this.state.isShowLangDropDown;
        this.setState({
            isShowLangDropDown: temp
        });
    }

    logout = () => {
        localStorage.clear();
        let value = this.context;
        value.setLogin(false);
    }

    render() {
        let account = JSON.parse(localStorage.getItem("user"))?.account;
        return <>
            <BaseContext.Consumer>
                {v => <HeaderStyle>
                    <Link to="/home">Logo</Link>

                    <NameDiv>
                        {v.isLoggin ? <div> <p>Hello, {account} <i className="fa-solid fa-right-from-bracket" onClick={() => {this.logout()}}></i></p> </div> : <Link to="/login">Login</Link>}
                        &nbsp;&nbsp;
                        <i className="fa-solid fa-globe" onClick={this.showLanguageDropDown}></i>
                    </NameDiv>

                    {this.state.isShowLangDropDown ? <DropDownLanguage>
                        <SelectBox name="language" id="language" value={v.lang} onChange={(e) => { this.languageChangeValue(e) }}>
                            <option value="en">English</option>
                            <option value="jp">Japanese</option>
                            <option value="vn">VietNamese</option>
                        </SelectBox>
                    </DropDownLanguage> : null}



                </HeaderStyle>}
            </BaseContext.Consumer>
            {/* <BaseContext.Consumer>
            {
                value => <></>
            }
            </BaseContext.Consumer> */}
        </>

    }
}

export default Header1;