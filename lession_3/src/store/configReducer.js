import { changeContentConfigAction } from "./actions/configActionType";

const initValue = {
    content: "this is data",
    numberFunc: 9,
    nums: [1, 13, 7, 19, 8, 11, 22, 1, 9, 94, 1, 5, 98]
}


// action 1 change content bằng 1 giá trị khác
// 2. tăng numberFunc lên 1
// 3. giảm numberFunc đi 1
// 4. thay thế numberFunc bằng 1 số khác
// 5. thêm 1 phần tử vào nums
// 6. xoá 1 phần tử trong nums
// 7. thay thế 1 phần tử trong nums

function configReducer(state=initValue, action) {
    switch(action.type) {
        case changeContentConfigAction: 
            return {
                ...state,
                content: action.payload
            };
        case "config/increNumberFunc": 
            return {
                ...state,
                numberFunc: Number(state.numberFunc) + 1
            };
        case "config/decreNumberFunc": 
            return {
                ...state,
                numberFunc: Number(state.numberFunc) - 1
            };
        case "config/changeNumberFunc": 
            return {
                ...state,
                numberFunc: Number(action.payload)
            };

        case "config/addNums": 
            return {
                ...state,
                nums: [...state.nums, action.payload]
            };
        case "config/deleteNums": 
            const numberTemp = [...state.nums];
            const tempDelete = [];
            for (let index = 0; index < numberTemp.length; index++) {
                if(numberTemp[index] != action.payload) {
                    tempDelete.push(numberTemp[index])
                }  
            }
            // for (const e of numberTemp) {
            //     if (e != action.payload) {
            //         tempDelete.push(e)
            //     }
            // }      
            return {
                ...state,
                nums: tempDelete
            };

        case "config/changeNums": 
            const number = [...state.nums];
            let tempChange = [];
            for (const e of number) {
                if(e == action.payload.a1) {
                    tempChange.push(action.payload.bcd);
                } else {
                    tempChange.push(e)
                }

            }
            // const indexChange = state.nums.indexOf(action.payload.location);
            // const tempChange = number.splice(indexChange, 1, action.payload.value);      
            return {
                ...state,
                nums: tempChange
            };
        default:
            return state;
    }
}

export default configReducer