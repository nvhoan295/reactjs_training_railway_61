import { createSlice } from "@reduxjs/toolkit";
import { authAction } from "../actions/authActionType";
import { loginApiAction } from "../actions/authAction";

const authSlice = createSlice({
  name: authAction,
  initialState: {
    status: "",
    token: "",
    user: {
      id: -1,
      account: "",
      password: "",
      email: "",
      full_name: "",
      status: "",
      role: "",
      permissions: "",
      nationality: "",
    },
    // test: ()=>{}
  },
  reducers: {
    logoutAction: (state, action) => {
      localStorage.clear();
      state.status = "";
      state.token = "";
      state.user = {
        id: -1,
        account: "",
        password: "",
        email: "",
        full_name: "",
        status: "",
        role: "",
        permissions: "",
        nationality: "",
      };
    },
  },
  extraReducers: {
    [loginApiAction.pending]: (state, action) => {
      console.log("extraReducers-pending", action);
      state.status = "pending";
    },
    [loginApiAction.fulfilled]: (state, action) => {
      console.log("extraReducers-fulfilled", action);
      state.status = "fulfilled";
      state.token = action.payload.token;
      state.user = action.payload.user;
      localStorage.setItem("token", action.payload.token);
      localStorage.setItem("user", JSON.stringify(action.payload.user));
    },
    [loginApiAction.rejected]: (state, action) => {
      console.log("extraReducers-rejected", action);
      state.status = "rejected";
    },
  },
});
export const { logoutAction } = authSlice.actions;
export default authSlice.reducer;
