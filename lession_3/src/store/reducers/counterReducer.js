import { createSlice } from "@reduxjs/toolkit";
import { counterAction } from "../actions/counterActionType";
import { getValue1Api } from "../actions/counterAsynAction";

const counterSlice = createSlice({
    name: counterAction,
    initialState: {
        value: 0,
        value1: 0
    },
    reducers: {
        increment: (state) => {
            state.value += 1 
        },
        decrement: (state) => {
            state.value -= 1 
        },
        incrementByAmount: (state, action) => {
            state.value += Number(action.payload)
        },
    },
    extraReducers: {
        [getValue1Api.pending]: (state, action) => {
            state.value1 = 0;
        },
        [getValue1Api.fulfilled]: (state, action) => {
            if(isNaN(action.payload)) {
                state.value1 = 0;
            } else {
                state.value1 = Number(action.payload);
            }
            
        },
        [getValue1Api.rejected]: (state, action) => {
            state.value1 = 0;
        },
    }
});

export const {increment, decrement, incrementByAmount} = counterSlice.actions;

export default counterSlice.reducer;