import { createAsyncThunk } from "@reduxjs/toolkit";
import { callAPIAuthAction } from "./authActionType";
import api from "../../services/api";
import { BASE_URL } from "../../services/baseService";

export const loginApiAction = createAsyncThunk(
    callAPIAuthAction, 
    async (loginValue, {dispatch, getState, rejectWithValue, fulfillWithValue}) => {
        console.log("loginValue", loginValue);
        console.log(getState());
        let res = await api.postLogin(BASE_URL + "/auth", loginValue)
        .then(
            (response) => {
                // throw new Error("nhay vao case catch");
                console.log("response", response);
                if(response && response.token) {
                    return response;
                } else {
                    return rejectWithValue({errors: "login fail"});
                }
                
            },
            (errors) => {
                console.log("errors", errors);
                return rejectWithValue({errors: errors});
            }
        )
        .catch(
            (errors) => {
                console.log("errors-catch", errors);
                return rejectWithValue({errors: errors});
            }
        );
        return res;
    }, 
    {}
);
