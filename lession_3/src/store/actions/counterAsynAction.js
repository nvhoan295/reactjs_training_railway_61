import { createAsyncThunk } from "@reduxjs/toolkit";
import { incrementByAmount } from "../reducers/counterReducer"
import { callApiValue1Action } from "./counterActionType";
import api from "../../services/api";
import { BASE_URL } from "../../services/baseService";

const incrementInSetTimeOut = (dispatch, amount) => {
    dispatch(incrementByAmount(amount));
}

export const incrementAsyn = (data, funcObj) => {
    // setTimeout(incrementInSetTimeOut, 5000);
    setTimeout(() => {
        funcObj(incrementByAmount(data));
    }, 1000);
}

export const getValue1Api = createAsyncThunk(
    callApiValue1Action,
     async (param, {getState, dispatch, rejectWithValue,fulfillWithValue}) => {
        let respose = await api.get(BASE_URL + '/counter/value1')
        .then(
            (response) => {
                // throw new Error("nhay vao case catch");
                console.log("response", response);
                if(response) {
                    return response;
                } else {
                    return rejectWithValue({errors: "login fail"});
                }
                
            },
            (errors) => {
                console.log("errors", errors);
                return rejectWithValue({errors: errors});
            }
        )
        .catch(
            (errors) => {
                console.log("errors-catch", errors);
                return rejectWithValue({errors: errors});
            }
        );
        if (!isNaN(respose)) {
            dispatch(incrementByAmount(respose))
        }
        
        return respose;
    },
    {}
);