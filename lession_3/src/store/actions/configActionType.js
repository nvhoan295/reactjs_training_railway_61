export const configAction = 'config';
export const changeContentConfigAction = configAction + '/changeContent';
export const increNumberFuncConfigAction = configAction + '/increNumberFunc';
export const decreNumberFuncConfigAction = configAction + '/decreNumberFunc';
export const changeNumberFuncConfigAction = configAction + '/changeNumberFunc';
export const addNumsConfigAction = configAction + '/addNums';
export const deleteNumsConfigAction = configAction + '/deleteNums';
export const changeNumsConfigAction = configAction + '/changeNums';