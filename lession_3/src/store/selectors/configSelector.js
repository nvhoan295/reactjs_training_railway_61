export const configSelector = state => state.config;
export const configContentSelector = state => state.config.content;
export const configNumberFuncSelector = state => state.config.numberFunc;
export const configNumsSelector = state => state.config.nums;

