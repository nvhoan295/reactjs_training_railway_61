const initValue = {
    token: "",
    user: "",
}



function authReducerOld(state=initValue, action) {
    switch(action.type) {
        case "auth/changeToken": 
            return {
                ...state,
                token: action.payload
            };
        case "auth/changeUser": 
            return {
                ...state,
                user: action.payload
            };
        default:
            return state;
    }
}

export default authReducerOld;