import { combineReducers, createStore } from "redux";
import configReducer from "./configReducer";
import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "./reducers/counterReducer";
import authReducer from "./reducers/authReducer";
import { 
  persistReducer, 
  persistStore, 
  FLUSH, 
  PERSIST, 
  PURGE, 
  PAUSE, 
  REHYDRATE, 
  REGISTER 
} from "redux-persist";
import storage from 'redux-persist/lib/storage';

const rootReducer = combineReducers({
  config: configReducer,
  authen: authReducer,
  counter: counterReducer
});

const persistConfig = {
  key: 'root',
  storage: storage,
  blacklist: ['counter', 'config']
};

const pReducer = persistReducer(persistConfig, rootReducer);

// const rootStore = createStore(
//   rootReducer,
//   window.__REDUX_DEVTOOLS_EXTENSION__ &&
//     window.__REDUX_DEVTOOLS_EXTENSION__({ trace: true })
// );

const rootStore = configureStore({
  reducer: pReducer,
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware({
      serializableCheck: {
        ignoreActions: [FLUSH, PERSIST, PURGE, PAUSE, REHYDRATE, REGISTER]
      }
    })
  }
});

export default rootStore;
export const persistor = persistStore(rootStore);
